@extends('layout.master')
@section('judul')
halaman LIst Cast
    
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm mb-4">Tambah Cast </a>

<table class="table table bordered">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">bio</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
            <tr>
                <td> {{$key +1}} </td>
                <td> {{$item->nama }} </td>
                <td> {{$item->umur }} </td>
                <td>
                  
                  <form action="/cast/{{$item->id}}"method="POST">
                    @csrf
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit</a>
                    @method('delete')
                    <input type="submit" value="delete" class="btn btn-danger bts-sm">
                  </form>
                </td>
            </tr>





        @empty
                <tr>
                    <td> tidak ada data </td>
                </tr>

            
       
      @endforelse
    
    </tbody>
  </table>
    
@endsection