@extends('layout.master')
@section('judul')
Halaman Data Table
    
@endsection
@push('script')

<script src="{{asset('template/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
    
@endpush

@section('content')
@include('page.datatable')


@endsection

  