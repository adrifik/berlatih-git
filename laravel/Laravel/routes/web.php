<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home' );
Route::get('/table', 'AuthController@pagetable' );
Route::get('/datatable', 'AuthController@pagedatatable' );

//untuk masuk ke form category
Route::get('/cast/create', 'CastController@create');

Route::post('/cast', 'CastController@store');

Route::get('/cast', 'CastController@index');
Route::get('/cast/{id}', 'CastController@show');

//update data


//masuk ke form based on cast id
Route::get('/cast/{id}/edit', 'CastController@edit');

//untuk update data based on id
Route::put('/cast/{id}', 'CastController@update');

//untuk delete data
Route::delete('/cast/{id}', 'CastController@destroy');






