<?php
require_once("animal.php");
require_once("Frog.php");
require_once("Ape.php");

$sheep = new Animal("shaun");

echo "nama : " .$sheep->hewan."<br>"; 
echo "legs : " .$sheep->legs."<br>"; 
echo "cold booded : ". $sheep->cold_blooded."<br><br>"; 

$sungokong = new Ape("kera sakti");
echo "nama : " .$sungokong->hewan."<br>"; 
echo "legs : " .$sungokong->legs."<br>"; 
echo "cold booded : ". $sungokong->cold_blooded."<br>"; 
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
echo "nama : " .$kodok->hewan."<br>"; 
echo "legs : " .$kodok->legs."<br>"; 
echo "cold booded : ". $kodok->cold_blooded."<br>"; 
$kodok->jump() ; // "hop hop"

?>